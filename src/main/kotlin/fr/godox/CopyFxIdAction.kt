package fr.godox

import fr.godox.AFxmlAction.TemplateType.FIELD
import java.io.File

class CopyFxIdAction : AFxmlAction() {

    override val templateType: TemplateType = FIELD

    override fun onFxmlClicked(fxmlFile: File) {
        val fxmlIds = captureFxIdGroups(fxmlFile.readText()).joinToString(
            transform = { getTemplate().replace("\${type}", it.first).replace("\${var}", it.second) },
            separator = "\n"
        )
        if (fxmlIds.isNotEmpty()) {
            addToClipboard(fxmlIds)
            showCopiedMessage(fxmlFile.name)
        } else {
            showEmptyMessage(fxmlFile.name)
        }
    }

    private fun captureTypeField(xmlLine: String): String {
        val regexPattern = "type=\"(\\w+)\""
        return Regex(regexPattern, RegexOption.MULTILINE).find(xmlLine)?.groupValues?.get(1) ?: "Node"
    }

    private fun captureFxIdGroups(file: String): List<Pair<String, String>> {
        val regexPattern = "<(\\w+)\\s*.*\\s*fx:id=\"(\\w+)\"[\\s\\S]*?>"
        val result = Regex(regexPattern, RegexOption.MULTILINE).findAll(file)
        return result.map {
            val type = (if (it.groupValues[1] == "fx") captureTypeField(it.groupValues[0]) else it.groupValues[1])
            type to it.groupValues[2]
        }.toList()
    }

}