package fr.godox

import com.intellij.notification.Notification
import com.intellij.notification.NotificationType
import com.intellij.notification.Notifications
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import java.awt.Toolkit
import java.awt.datatransfer.StringSelection
import java.io.File
import java.io.FileNotFoundException

abstract class AFxmlAction : AnAction() {

    lateinit var event: AnActionEvent

    abstract val templateType: TemplateType

    override fun actionPerformed(e: AnActionEvent) {
        this.event = e
        onFxmlClicked(File(e.getData(CommonDataKeys.VIRTUAL_FILE)!!.path))
    }

    fun getTemplate(): String {
        val language = event.presentation.text.lowercase()
        val templateType = templateType.name.lowercase()
        return CopyFxMethodAction::class.java.getResource("/templates/$templateType/$language.tpl")
            ?.readText()
            ?: throw FileNotFoundException("Unable to find $language.tpl $templateType template")
    }

    fun showEmptyMessage(fxmlFileName: String) =
        Notifications.Bus.notify(
            Notification(
                "fr.godox.fxidclipboarder",
                "Empty",
                "No $templateType found in $fxmlFileName",
                NotificationType.WARNING
            )
        )

    fun showCopiedMessage(fxmlFileName: String) =
        Notifications.Bus.notify(
            Notification(
                "fr.godox.fxidclipboarder",
                "Copied",
                "Copied ${templateType}s of $fxmlFileName to clipboard",
                NotificationType.INFORMATION
            )
        )

    fun addToClipboard(fxmlIds: String) {
        Toolkit.getDefaultToolkit().systemClipboard.setContents(StringSelection(fxmlIds), null)
    }

    fun <T> getUserSelected(fxmlMethods: List<T>, cellFactory: (T) -> String): Collection<T> {
        SelectorPanel(fxmlMethods) { cellFactory(it) }.apply {
            show()
            return this.getResult()
        }
    }

    abstract fun onFxmlClicked(fxmlFile: File)

    enum class TemplateType {
        METHOD, FIELD;

        override fun toString(): String = this.name.lowercase()
    }

    override fun update(e: AnActionEvent) {
        e.presentation.isEnabledAndVisible = "fxml" == (e.getData(CommonDataKeys.VIRTUAL_FILE)?.extension ?: "")
    }

}