package fr.godox

import fr.godox.AFxmlAction.TemplateType.METHOD
import java.io.File

class CopyFxMethodAction : AFxmlAction() {

    override val templateType: TemplateType = METHOD

    override fun onFxmlClicked(fxmlFile: File) {
        val capturedFxMethodGroups = captureFxMethodGroups(fxmlFile.readText())
        val fxmlMethods = getUserSelected(capturedFxMethodGroups) { it }.joinToString(
            transform = { getTemplate().replace("\${name}", it) },
            separator = "\n"
        )
        if (fxmlMethods.isNotEmpty()) {
            addToClipboard(fxmlMethods)
            showCopiedMessage(fxmlFile.name)
        } else {
            showEmptyMessage(fxmlFile.name)
        }
    }

    private fun captureFxMethodGroups(file: String): List<String> {
        val regexPattern = "<\\s*.*\\s*on[A-Z]\\w+=\"#(\\w+)\"[\\s\\S]*?>"
        val result = Regex(regexPattern, RegexOption.MULTILINE).findAll(file)
        return result.map { it.groupValues[1] }.toList()
    }

}

