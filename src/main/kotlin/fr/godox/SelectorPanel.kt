package fr.godox

import com.intellij.openapi.ui.DialogWrapper
import java.awt.BorderLayout
import java.awt.Color
import java.awt.Dimension
import java.awt.event.ActionEvent
import java.lang.Boolean
import javax.swing.*
import javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION
import javax.swing.border.EmptyBorder
import javax.swing.border.LineBorder
import kotlin.Array
import kotlin.String
import kotlin.apply
import kotlin.arrayOf


class SelectorPanel<T>(private val selection: Collection<T>, private val cellFactory: (T) -> String) : DialogWrapper(true) {

    lateinit var jList: JList<String>

    init {
        title = "Select your values"
        init()
    }

    override fun createActions(): Array<Action>? {
        super.createDefaultActions()
        val selectAllAction = object : AbstractAction("Select All") {

            init {
                putValue(FOCUSED_ACTION, Boolean.TRUE)
            }

            override fun actionPerformed(p0: ActionEvent?) {
                jList.selectAll()
                doOKAction()
            }
        }
        return arrayOf(selectAllAction, okAction, cancelAction)
    }

    fun getResult(): Collection<T> = selection.filter {
        cellFactory(it) in jList.selectedValuesList
    }

    override fun createJButtonForAction(action: Action?): JButton {
        return super.createJButtonForAction(action)
    }

    override fun createCenterPanel(): JComponent? {
        val dialogPanel = JPanel(BorderLayout())
        jList = JList(selection.map { cellFactory(it) }.toTypedArray()).apply {
            border = LineBorder(Color.GRAY, 2, true)
            selectionMode = MULTIPLE_INTERVAL_SELECTION
            preferredSize = Dimension(400, 500)
            this.selectAll()
        }
        dialogPanel.add(jList, BorderLayout.CENTER)
        dialogPanel.border = EmptyBorder(10, 10, 10, 10)
        return dialogPanel
    }

}

private fun <T> JList<T>.selectAll() {
    selectedIndices = (0..model.size).toList().toIntArray()
}
